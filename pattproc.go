package comprfind

import "errors"

// substr is a way of representing substrings
// of a pattern by two numbers - start and end
// of a substring.
type substr struct {
	s uint16
	e uint16
}

type pattproc struct {
	P []byte
	// m is a length of P - the pattern.
	m uint16
	// N2 is an auxiliary arrays for answering
	// Q2 queries. N2[i][j] is maximum l s.t.
	// P[i-l:i]·P[j:j+m-l] == P
	N2 [][]uint16
	// PC[i][j] is a lowest l s.t. P[l:] is
	// a suffix of P[i:j].
	PC [][]uint16
	// st is an uncompressed suffix tree of P.
	st *sufftree
	// sp for an internal substring of P stores
	// a pointer to corresponding vertex in st.
	sp [][]*sufftree
}

// Q1 returns the length of the longest pattern prefix
// that is a suffix of s1·s2, when s1 is a pattern prefix
// and s2 - an internal substring.
func (p *pattproc) Q1(s1, s2 substr) (uint16, error) {
	if s1.s != 0 {
		return 0, errors.New("Q1: s1 not a prefix")
	}
	if p.sp[s2.s][s2.e].N1[s1.e] != 0 {
		return p.sp[s2.s][s2.e].N1[s1.e] + (s2.e - s2.s), nil
	}
	return p.PC[s2.s][s2.e], nil
}

// Q2 returns lowest i s.t. P is a prefix of s1·s2[i:],
// where s1 is a prefix of P, and s2 is a suffix.
func (p *pattproc) Q2(s1, s2 substr) (uint16, error) {
	if s1.s != 0 || s2.e != p.m {
		return 0, errors.New("Q2: Either S1 is not a prefix or S2 not a suffix")
	}
	if p.N2[s1.e][s2.s] != 0 {
		return s1.e - p.N2[s1.e][s2.s], nil
	}
	return s1.e + (s2.e - s2.s), nil
}

// Q3 returns latest <i, j> s.t. s·a = P[i..j].
// If none ─ return <0, 0>.
func (p *pattproc) Q3(s substr, a byte) substr {
	v := p.sp[s.s][s.e]
	if v.children[a] != nil {
		u := v.children[a]
		j := u.repr
		i := j - (s.e - s.s) - 1
		return substr{s: i, e: j}
	}
	return substr{0, 0}
}

// newPattProc creates a new structer with
// a preprocessed pattern
func newPattProc(P []byte, litWidth int) *pattproc {
	pp := new(pattproc)
	pp.P = P
	pp.m = uint16(len(P))
	pp.st, pp.sp = newSuffTree(P, litWidth)

	Pi := prefixFunction(P)
	// generate PC using Pi
	pp.PC = make([][]uint16, pp.m)
	for i := uint16(0); i < pp.m; i++ {
		pp.PC[i] = make([]uint16, pp.m+1)
	}
	for j := uint16(1); j <= pp.m; j++ {
		k := Pi[j-1]
		pp.PC[0][j] = j
		for i := uint16(1); i < j; i++ {
			for k > j-i {
				k = Pi[k]
			}
			pp.PC[i][j] = k
		}
	}
	// Fix N1's in suftree using Pi
	for i := uint16(1); i < pp.m; i++ {
		for j := uint16(0); j < pp.m; j++ {
			for k := j; k <= pp.m; k++ {
				if pp.sp[j][k].N1[i] == 0 {
					pp.sp[j][k].N1[i] = pp.sp[j][k].N1[Pi[i-1]]
				}
			}
		}
	}
	// Create N2
	pp.N2 = make([][]uint16, pp.m)
	for i := uint16(0); i < pp.m; i++ {
		pp.N2[i] = make([]uint16, pp.m)
		for j := uint16(0); j < pp.m; j++ {
			if i == j {
				pp.N2[i][j] = i
			} else if j < i && pp.sp[j][j+(pp.m-i)].N1[i] == i {
				pp.N2[i][j] = i
			} else if i != 0 {
				pp.N2[i][j] = pp.N2[Pi[i-1]][j]
			}
		}
	}
	return pp
}

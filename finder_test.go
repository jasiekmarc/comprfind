package comprfind

import (
	"bytes"
	"compress/lzw"
	"io"
	"io/ioutil"
	"os"
	"runtime"
	"strconv"
	"strings"
	"testing"
)

type comprfindtest struct {
	desc    string
	raw     string
	pattern string
	found   bool
	err     error
}

var compfindtests = []comprfindtest{
	{
		"small;LSB;8",
		"TOBETOTOBE",
		"BET",
		true,
		nil,
	},
	{
		"small;LSB;8",
		"TOBETOTOBE",
		"BETE",
		false,
		nil,
	},
	{
		"tobe;LSB;7",
		"TOBEORNOTTOBEORTOBEORNOT",
		"RTOB",
		true,
		nil,
	},
	{
		"tobe;LSB;7",
		"TOBEORNOTTOBEORTOBEORNOT",
		"RTBB",
		false,
		nil,
	},
	{
		"diac;LSB;8",
		"POZWOLĘSOBIEUBIĆKRÓWKĘ",
		"RÓW",
		true,
		nil,
	},
	{
		"diac;LSB;8",
		"POZWOLĘSOBIEUBIĆKRÓWKĘ",
		"UBIĘ",
		false,
		nil,
	},
}

func TestLZWFind(t *testing.T) {
	for _, tt := range compfindtests {
		d := strings.Split(tt.desc, ";")
		var order lzw.Order
		switch d[1] {
		case "LSB":
			order = lzw.LSB
		case "MSB":
			order = lzw.MSB
		default:
			t.Errorf("%s: bad order %q", tt.desc, d[1])
		}
		litWidth, _ := strconv.Atoi(d[2])
		piper, pipew := io.Pipe()
		defer piper.Close()
		go func() {
			defer pipew.Close()
			lzww := lzw.NewWriter(pipew, order, litWidth)
			defer lzww.Close()
			lzww.Write([]byte(tt.raw))
		}()
		comp, _ := ioutil.ReadAll(piper)
		found, err := MatchBytes([]byte(tt.pattern), comp, order, litWidth)
		if found != tt.found {
			t.Errorf("LZWFinder: wrong answer for text: %s, pattern: %s", tt.raw, tt.pattern)
		}
		if err != tt.err {
			t.Error("LZWFinder: wrong error returned", err, "instead of", tt.err)
		}
	}
}

func TestBigFileNotFound(t *testing.T) {
	f, err := os.Open("testdata/pt.Z")
	if err != nil {
		t.Fatal(err)
	}
	pattern := []byte("zubiję")
	found, err := MatchReader(pattern, f, lzw.LSB, 8)
	if found != false {
		t.Error("Wrong answer")
	}
	if err != nil {
		t.Error(err)
	}
}

func TestBigFileFound(t *testing.T) {
	f, err := os.Open("testdata/pt.Z")
	if err != nil {
		t.Fatal(err)
	}
	pattern := []byte("skazki")
	found, err := MatchReader(pattern, f, lzw.LSB, 8)
	if found != true {
		t.Error("Wrong answer")
	}
	if err != nil {
		t.Error(err)
	}
}

func BenchmarkFindLZWPatternNotFound(b *testing.B) {
	b.StopTimer()
	s, err := ioutil.ReadFile("testdata/pt.Z")
	if err != nil {
		b.Fatal(err)
	}
	if len(s) == 0 {
		b.Fatalf("test file has no data")
	}
	pattern := []byte("zubiję")
	runtime.GC()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		_, _ = MatchBytes(pattern, s, lzw.LSB, 8)
	}
}

func BenchmarkUnpackAndSearchNotFound(b *testing.B) {
	b.StopTimer()
	s, err := ioutil.ReadFile("testdata/pt.Z")
	if err != nil {
		b.Fatal(err)
	}
	if len(s) == 0 {
		b.Fatalf("test file has no data")
	}
	pattern := "zubiję"
	runtime.GC()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		lzwr := lzw.NewReader(bytes.NewBuffer(s), lzw.LSB, 8)
		unco, _ := ioutil.ReadAll(lzwr)
		_ = strings.Contains(string(unco), pattern)
	}
}

func BenchmarkFindLZWPatternFound(b *testing.B) {
	b.StopTimer()
	s, err := ioutil.ReadFile("testdata/pt.Z")
	if err != nil {
		b.Fatal(err)
	}
	if len(s) == 0 {
		b.Fatalf("test file has no data")
	}
	pattern := []byte("skazki")
	runtime.GC()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		_, _ = MatchBytes(pattern, s, lzw.LSB, 8)
	}
}

func BenchmarkUnpackAndSearchFound(b *testing.B) {
	b.StopTimer()
	s, err := ioutil.ReadFile("testdata/pt.Z")
	if err != nil {
		b.Fatal(err)
	}
	if len(s) == 0 {
		b.Fatalf("test file has no data")
	}
	pattern := "skazki"
	runtime.GC()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		lzwr := lzw.NewReader(bytes.NewBuffer(s), lzw.LSB, 8)
		unco, _ := ioutil.ReadAll(lzwr)
		_ = strings.Contains(string(unco), pattern)
	}
}

func BenchmarkSmallFindLZWPattern(b *testing.B) {
	b.StopTimer()
	tt := compfindtests[len(compfindtests)-1]
	d := strings.Split(tt.desc, ";")
	var order lzw.Order
	switch d[1] {
	case "LSB":
		order = lzw.LSB
	case "MSB":
		order = lzw.MSB
	default:
		b.Fatalf("%s: bad order %q", tt.desc, d[1])
	}
	litWidth, _ := strconv.Atoi(d[2])
	piper, pipew := io.Pipe()
	defer piper.Close()
	go func() {
		defer pipew.Close()
		lzww := lzw.NewWriter(pipew, order, litWidth)
		defer lzww.Close()
		lzww.Write([]byte(tt.raw))
	}()
	comp, _ := ioutil.ReadAll(piper)
	patt := []byte(tt.pattern)
	runtime.GC()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		_, _ = MatchBytes(patt, comp, order, litWidth)
	}
}

func BenchmarkSmallUnpackAndSearch(b *testing.B) {
	b.StopTimer()
	tt := compfindtests[len(compfindtests)-1]
	d := strings.Split(tt.desc, ";")
	var order lzw.Order
	switch d[1] {
	case "LSB":
		order = lzw.LSB
	case "MSB":
		order = lzw.MSB
	default:
		b.Fatalf("%s: bad order %q", tt.desc, d[1])
	}
	litWidth, _ := strconv.Atoi(d[2])
	piper, pipew := io.Pipe()
	defer piper.Close()
	go func() {
		defer pipew.Close()
		lzww := lzw.NewWriter(pipew, order, litWidth)
		defer lzww.Close()
		lzww.Write([]byte(tt.raw))
	}()
	comp, _ := ioutil.ReadAll(piper)
	runtime.GC()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		lzwr := lzw.NewReader(bytes.NewBuffer(comp), order, litWidth)
		unco, _ := ioutil.ReadAll(lzwr)
		_ = strings.Contains(string(unco), tt.pattern)
	}
}

package comprfind

import "testing"

type qTest struct {
	P       string
	i, j, k uint16
	res     uint16
}

var q1tests = []qTest{
	{
		"abcabca",
		4, 1, 3,
		6,
	},
	{
		"abcab",
		4, 1, 4,
		4,
	},
	{
		"abcabca",
		3, 2, 5,
		2,
	},
}

func TestQ1(t *testing.T) {
	for _, tt := range q1tests {
		ptpr := newPattProc([]byte(tt.P), 8)
		s1 := substr{0, tt.i}
		s2 := substr{tt.j, tt.k}
		ans, err := ptpr.Q1(s1, s2)
		if err != nil {
			t.Error("Q1 failed on test ", tt)
		} else if ans != tt.res {
			t.Errorf("Q1: wrong answer (%d instead of %d)", ans, tt.res)
			t.Errorf("\tN1[%d,%d,%d] = %d", tt.i, tt.j, tt.k, ptpr.sp[tt.j][tt.k].N1[tt.i])
		}
	}
}

var q2tests = []qTest{
	{
		"abcab",
		4, 1, 5,
		0,
	},
	{
		"abcab",
		4, 3, 5,
		6,
	},
}

func TestQ2(t *testing.T) {
	for _, tt := range q2tests {
		ptpr := newPattProc([]byte(tt.P), 8)
		s1 := substr{0, tt.i}
		s2 := substr{tt.j, tt.k}
		ans, err := ptpr.Q2(s1, s2)
		if err != nil {
			t.Error("Q2 failed on test ", tt)
		} else if ans != tt.res {
			t.Errorf("Q2: wrong answer (%d instead of %d)", ans, tt.res)
			t.Errorf("\tN2[%d,%d] = %d", tt.i, tt.j, ptpr.N2[tt.i][tt.j])
		}
	}
}

type q3Test struct {
	P    string
	i, j uint16
	a    byte
	res  substr
}

var q3tests = []q3Test{
	{
		"abcabca",
		5, 7,
		'b',
		substr{2, 5},
	},
	{
		"abcabca",
		1, 3,
		'a',
		substr{4, 7},
	},
	{
		"abcabca",
		3, 6,
		'b',
		substr{0, 0},
	},
	{
		"bet",
		0, 2,
		't',
		substr{0, 3},
	},
}

func TestQ3(t *testing.T) {
	for _, tt := range q3tests {
		ptpr := newPattProc([]byte(tt.P), 8)
		s := substr{tt.i, tt.j}
		ans := ptpr.Q3(s, tt.a)
		if ans != tt.res {
			t.Error("Q3: wrong answer -", ans, "instead of", tt.res)
		}
	}
}

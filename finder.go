// Package comprfind provides an implementation of the basic
// O(n + m^3) version of Amir-Benson-Farach algorithm for finding
// patterns in lzw-compressed text without uncompressing it.
package comprfind

import (
	"bufio"
	"bytes"
	"compress/lzw"
	"errors"
	"fmt"
	"io"
)

const (
	maxWidth           = 12
	decoderInvalidCode = 0xffff
	flushBuffer        = 1 << maxWidth
)

type dictentry struct {
	// Every node in trie has representing prefix (and suffix),
	// which are nonzero only if the chunk is prefix chunk
	// (or suffix chunk, respectively).
	pref, suff uint16
	// Similarly, internal flag shows, whether the chunk
	// represents a substring of the pattern.
	intrn substr
	// First and last character of chunk represented by a node.
	first, label byte
	// Parent of node in trie.
	parent *dictentry
}

type comprfinder struct {
	r        io.ByteReader
	bits     uint32
	nBits    uint
	width    uint
	read     func(*comprfinder) (uint16, error) // readLSB or readMSB
	litWidth int                                // width in bits of literal codes
	err      error
	found    bool
	// The first 1<<litWidth codes are literal codes.
	// The next two codes mean clear and EOF.
	// Other valid codes are in the range [lo, hi] where lo := clear + 2,
	// with the upper bound incrementing on each code seen.
	// overflow is the code at which hi overflows the code width.
	// last is the most recently seen code, or decoderInvalidCode.
	clear, eof, hi, overflow, last uint16
	// pp is the structure for preprocessing the pattern.
	pp pattproc
	// dict acts as a counterpart for trie.
	dict [1 << maxWidth]dictentry
	// prefix tells us what we found so far.
	prefix uint16
}

// readLSB returns the next code for "Least Significant Bits first" data.
func (c *comprfinder) readLSB() (uint16, error) {
	for c.nBits < c.width {
		x, err := c.r.ReadByte()
		if err != nil {
			return 0, err
		}
		c.bits |= uint32(x) << c.nBits
		c.nBits += 8
	}
	code := uint16(c.bits & (1<<c.width - 1))
	c.bits >>= c.width
	c.nBits -= c.width
	return code, nil
}

// readMSB returns the next code for "Most Significant Bits first" data.
func (c *comprfinder) readMSB() (uint16, error) {
	for c.nBits < c.width {
		x, err := c.r.ReadByte()
		if err != nil {
			return 0, err
		}
		c.bits |= uint32(x) << (24 - c.nBits)
		c.nBits += 8
	}
	code := uint16(c.bits >> (32 - c.width))
	c.bits <<= c.width
	c.nBits -= c.width
	return code, nil
}

// chunk checks if a pattern occurrence concludes in given chunk.
func (c *comprfinder) chunk(ana *dictentry) {
	switch {
	// case (a)
	case c.prefix == 0 && ana.pref != 0:
		c.prefix = ana.pref
	// case (b)
	case c.prefix != 0 && ana.suff != c.pp.m:
		r, err := c.pp.Q2(substr{0, c.prefix}, substr{ana.suff, c.pp.m})
		if err != nil {
			c.err = err
		}
		if r != c.prefix+(c.pp.m-ana.suff) {
			c.found = true
		}
	// case (c)
	case c.prefix != 0:
		if ana.intrn.e != 0 {
			var err error
			c.prefix, err = c.pp.Q1(substr{0, c.prefix}, ana.intrn)
			if err != nil {
				c.err = err
			}
		} else {
			c.prefix = ana.pref
		}
	}

}

func (c *comprfinder) find() {
	for {
		code, err := c.read(c)
		if err != nil {
			if err == io.EOF {
				err = io.ErrUnexpectedEOF
			}
			c.err = err
			return
		}
		switch {
		case code == c.clear:
			c.width = 1 + uint(c.litWidth)
			c.hi = c.eof
			c.overflow = 1 << c.width

			c.last = decoderInvalidCode
			if c.found {
				return
			}
			continue
		case code == c.eof:
			c.err = io.EOF
			return
		case code <= c.hi:
			// create new node
			c.dict[c.hi+1] = dictentry{
				first:  c.dict[code].first,
				suff:   c.dict[code].suff,
				parent: &c.dict[code],
			}
			if c.last != decoderInvalidCode {
				// update previous node with now available information
				prev := &c.dict[c.hi]
				prev.label = c.dict[code].first
				// step (d)
				if prev.parent.intrn.e != 0 {
					prev.intrn = c.pp.Q3(prev.parent.intrn, prev.label)
					if prev.intrn.e == c.pp.m {
						prev.suff = prev.intrn.s
					}
				}
				// step (e)
				if c.dict[prev.label].intrn.e != 0 {
					prev.pref, _ = c.pp.Q1(substr{0, prev.parent.pref}, c.dict[prev.label].intrn)
				}
				if c.found {
					return
				}
			}
			c.chunk(&c.dict[code])
		default:
			c.err = errors.New("lzw: invalid code")
			return
		}
		c.last, c.hi = code, c.hi+1
		if c.hi >= c.overflow {
			if c.width == maxWidth {
				c.last = decoderInvalidCode
			} else {
				c.width++
				c.overflow <<= 1
			}
		}
	}
}

func newLZWFinder(r io.Reader, order lzw.Order, litWidth int, pattern []byte) *comprfinder {
	c := new(comprfinder)
	switch order {
	case lzw.LSB:
		c.read = (*comprfinder).readLSB
	case lzw.MSB:
		c.read = (*comprfinder).readMSB
	default:
		c.err = errors.New("lzw: unknown order")
		return c
	}
	if litWidth < 2 || 8 < litWidth {
		c.err = fmt.Errorf("lzw: litWidth %d out of range", litWidth)
		return c
	}
	if br, ok := r.(io.ByteReader); ok {
		c.r = br
	} else {
		c.r = bufio.NewReader(r)
	}

	c.litWidth = litWidth
	c.width = 1 + uint(litWidth)
	c.clear = uint16(1) << uint(litWidth)
	c.eof, c.hi = c.clear+1, c.clear+1
	c.overflow = uint16(1) << c.width
	c.last = decoderInvalidCode

	c.pp = *newPattProc(pattern, litWidth)
	for i := uint16(0); i < c.clear; i++ {
		if pattern[0] == byte(i) {
			c.dict[i].pref = 1
		}
		if pattern[len(pattern)-1] == byte(i) {
			c.dict[i].suff = c.pp.m - 1
		} else {
			c.dict[i].suff = c.pp.m
		}
		if c.pp.st.children[i] != nil {
			repr := c.pp.st.children[i].repr
			c.dict[i].intrn = substr{repr - 1, repr}
		} else {
			c.dict[i].intrn = substr{0, 0}
		}
		c.dict[i].first, c.dict[i].label = byte(i), byte(i)
	}
	return c
}

// MatchReader checks whether pattern matches the text read (in compressed form)
// by the Reader. Order specifies the bit ordering in the LZW data stream.
// The number of bits to use for literal codes, litWidth, must be in range
// [2, 8] and is typically 8. It must be the same as used for compression.
func MatchReader(pattern []byte, r io.Reader, order lzw.Order, litWidth int) (bool, error) {
	c := newLZWFinder(r, order, litWidth, pattern)
	c.find()
	if c.err != io.EOF {
		return c.found, c.err
	}
	return c.found, nil
}

// MatchBytes returns true if pattern appeares in uncompressed text.
// An error may be returned, when lzw compression is invalid. Order specifies
// the bit ordering in an LZW data stream. The number of bits to use
// for literal codes, litWidth must be in the range [2,8] and is usually 8.
func MatchBytes(pattern, b []byte, order lzw.Order, litWidth int) (bool, error) {
	return MatchReader(pattern, bytes.NewReader(b), order, litWidth)
}

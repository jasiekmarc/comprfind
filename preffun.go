package comprfind

// prefixFunction generates a prefix-function
// array as in KMP pattern finding algorithm.
// Pi[i] is a length of the longest nontrivial
// prefixo-suffix of P[:i].
func prefixFunction(P []byte) (Pi []uint16) {
	Pi = make([]uint16, len(P))
	var k uint16
	k = 0
	for i := 1; i < len(P); i++ {
		for k != 0 && P[k] != P[i] {
			k = Pi[k-1]
		}
		if P[k] == P[i] {
			k++
			Pi[i] = k
		}
	}
	return
}

[![GoDoc](https://godoc.org/bitbucket.org/jasiekmarc/comprfind?status.svg)](https://godoc.org/bitbucket.org/jasiekmarc/comprfind)
[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/jasiekmarc/comprfind)](https://goreportcard.com/report/bitbucket.org/jasiekmarc/comprfind)

The comprfind package is a project written for the “Data Compression” course
in 2013. It implements a O(n + m^3) version of Amir-Benson-Farach algorithm
for finding patterns in lzw-compressed text without uncompressing it.

The basic usage of the library would be:
```go
f, _ := os.Open("file.Z")
matched, _ := comprfind.MatchReader(pattern, f, lzw.LSB, 8)  // The options must match these used for compression.
```

The LZW format used by the package is compatible with this of `compress/lzw` package in Go standard library, and not with that of Unix standard `compress` program. This issue is discussed [on Stack Overflow](https://stackoverflow.com/a/42889912).
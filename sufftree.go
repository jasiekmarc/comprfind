package comprfind

// sufftree is an uncompressed suffix tree.
// To get a sufftree of a string, use the
// newSuffTree function.
type sufftree struct {
	// repr is an index in P (the pattern)
	// that the vertex in sufftree represents.
	repr uint16
	// children is an array of pointers to
	// children in the sufftree.
	children []*sufftree
	// N1[i] for an internal substring P[j:k] of P
	// is the largest l: s.t. P[(i-l):i]·P[j:k] is
	// a suffix of P
	N1 []uint16
}

// add modifies sufftree by adding a string
// (presumably one of suffices) to it.
func (s *sufftree) add(a, i uint16, P []byte, sp []*sufftree, litWidth int) {
	if i > s.repr {
		s.repr = i
	}
	sp[i] = s
	s.N1[a] = a
	if i == uint16(len(P)) {
		return
	}
	if s.children[P[i]] == nil {
		s.children[P[i]] = createNode(len(P), litWidth)
	}
	s.children[P[i]].add(a, i+1, P, sp, litWidth)
}

func createNode(m, litWidth int) (s *sufftree) {
	s = new(sufftree)
	s.N1 = make([]uint16, m)
	s.children = make([]*sufftree, 1<<uint(litWidth))
	return
}

func newSuffTree(P []byte, litWidth int) (*sufftree, [][]*sufftree) {
	s := createNode(len(P), litWidth)
	sp := make([][]*sufftree, len(P))
	for i := 0; i < len(P); i++ {
		sp[i] = make([]*sufftree, len(P)+1)
		s.add(uint16(i), uint16(i), P, sp[i], litWidth)
	}
	return s, sp
}
